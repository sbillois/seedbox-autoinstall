#!/bin/bash

echo "   _ __  _| |_   _| | ___   _ __ ___  _   _  __ _  ___ "
echo "  | '_ \| | | | | | |/ _ \ | '__/ _ \| | | |/ _  |/ _ \\"
echo "  | |_) | | | |_| | |  __/ | | | (_) | |_| | (_| |  __/"
echo "  | .__/|_|_|\__,_|_|\___| |_|  \___/ \__,_|\__, |\___|"
echo "  | |                                        __/ |     "
echo "  |_|                                       |___/      "


# Mettre à jour le système et installer Docker
echo "Mise à jour du système..."
sudo dnf update -y

echo "Installation de Docker..."
sudo dnf config-manager --add-repo=https://download.docker.com/linux/centos/docker-ce.repo
sudo dnf install docker-ce --nobest -y
sudo systemctl enable --now docker

# Vérification que Docker est bien installé
if ! command -v docker &> /dev/null; then
    echo "Docker n'a pas pu être installé correctement. Quitter..."
    exit 1
fi

# Création des dossiers
echo "Création des dossiers..."
mkdir -p ~/mediatheque/{films,livres,series,theatre}
mkdir -p ~/flood_config
mkdir -p ~/mediatheque/downloads
mkdir -p ~/jellyfin_config
mkdir -p ~/filebrowser_config
mkdir -p ~/komga_config

# Suppression des conteneurs existants s'ils existent déjà
CONTAINERS=(rtorrent-flood jellyfin filebrowser komga)
for CONTAINER in "${CONTAINERS[@]}"; do
    if [ $(sudo docker ps -aq -f name=$CONTAINER) ]; then
        echo "Suppression du conteneur existant : $CONTAINER"
        sudo docker stop $CONTAINER
        sudo docker rm $CONTAINER
    fi
done

# Lancement des conteneurs Docker
echo "Lancement des conteneurs Docker..."

# Conteneur rtorrent-flood
sudo docker run -d \
  --name rtorrent-flood \
  -v ~/flood_config:/config \
  -v ~/mediatheque/downloads:/data \
  -v ~/mediatheque:/medias \
  -v ~/mediatheque2:/medias2 \
  -p 333:3000 \
  -p 6881:6881 \
  -p 6881:6881/udp \
  jesec/rtorrent-flood

# Conteneur Jellyfin
sudo docker run -d \
  --name=jellyfin \
  -v ~/jellyfin_config:/config \
  -v ~/mediatheque:/media \
  -p 8096:8096 \
  jellyfin/jellyfin

# Conteneur FileBrowser
sudo docker run -d \
  --name=filebrowser \
  -v ~/filebrowser_config:/config \
  -v ~:/srv \
  -p 8080:80 \
  filebrowser/filebrowser

# Conteneur komga
sudo docker run -d \
  --name=komga \
  --user 1000:1000 \
  -p 25600:25600 \
  --mount type=bind,source=$HOME/komga_config,target=/config \
  --mount type=bind,source=$HOME/mediatheque,target=/data \
  --mount type=bind,source=$HOME/mediatheque2,target=/data2 \
  --restart unless-stopped \
  gotson/komga

echo "Script terminé!"

